var gulp = require('gulp');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');

var sassFiles = 'src/css/**/*.scss',
    cssDest = 'build/css/';

gulp.task('sass', function() {
 return gulp.src('src/css/**/*.scss') // Gets all files ending with .scss in app/scss and children dirs
 .pipe(sass())
 .pipe(gulp.dest('build/css'))
});

gulp.task('styles', function(){
    gulp.src(sassFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(cssDest));
});

// Images Task
gulp.task('images', function() {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/img'));
});

// Watch Task
gulp.task('watch', function(){
    gulp.watch('src/css/**/*.scss', gulp.series('sass'));
    gulp.watch('src/img/*', gulp.series('images'));
});

// Default Task
gulp.task('default', gulp.parallel('styles', 'images', 'watch'));